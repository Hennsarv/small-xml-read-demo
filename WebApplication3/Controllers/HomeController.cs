﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
using System.Xml.Linq;

public class InvItem
{
    public string Kood { get; set; }
    public string Kogus { get; set; }
    public string Hind { get; set; }
    public string Description { get; set; }

}

namespace WebApplication3.Controllers
{


    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            WebRequest web = WebRequest.Create("http://www.sarv.ee/ftp/henn/sql/arve.xml");
            var resp = web.GetResponse();
            var str = resp.GetResponseStream();
            StreamReader reader = new StreamReader(str);
            string vastus = reader.ReadToEnd();
            XDocument d = XDocument.Parse(vastus);
            var tulemus = d.Descendants("Item").Select(x => new InvItem { Kood = x.Attribute("Kood").Value, Kogus = x.Attribute("Kogus").Value, Hind = x.Attribute("Hind").Value, Description = x.Attribute("Description").Value     }).ToList();
            ViewBag.Vastus = tulemus;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}